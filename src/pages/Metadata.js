import React from "react"

import { Helmet } from "react-helmet"
import { graphql, StaticQuery } from "gatsby"

const Metadata = ({ title, desc, banner, pathname, meta }) => {
  return (
    <StaticQuery
      query={`${query}`}
      render={({
        site: {
          buildTime,
          siteMetadata: {
            siteUrl,
            defaultTitle,
            defaultDescription,
            defaultBanner,
            headline,
            siteLanguage,
            ogLanguage,
            author,
          },
        },
      }) => {
        const seo = {
          title: title || defaultTitle,
          description: desc || defaultDescription,
          image: `${siteUrl}${banner || defaultBanner}`,
          url: `${siteUrl}${pathname || ""}`,
        }

        // schema.org in JSONLD format
        // https://developers.google.com/search/docs/guides/intro-structured-data
        // You can fill out the 'author', 'creator' with more data or another type (e.g. 'Organization')

        const schemaOrgWebPage = {
          "@context": "http://schema.org",
          "@type": "WebPage",
          url: siteUrl,
          headline,
          inLanguage: siteLanguage,
          mainEntityOfPage: siteUrl,
          description: defaultDescription,
          name: defaultTitle,
          author: {
            "@type": "Person",
            name: author,
          },
          copyrightHolder: {
            "@type": "Person",
            name: author,
          },
          copyrightYear: "2020",
          creator: {
            "@type": "Person",
            name: author,
          },
          publisher: {
            "@type": "Person",
            name: author,
          },
          datePublished: "2020-18-11T04:40:00+11:00",
          dateModified: buildTime,
          image: {
            "@type": "ImageObject",
            url: `${siteUrl}${defaultBanner}`,
          },
        }

        return (
          <Helmet title={seo.title}>
            <html lang={siteLanguage} />
            <meta name="description" content={seo.description} />
            <meta name="image" content={seo.image} />
            <meta name="gatsby-starter" content="Gatsby Starter Hello World" />
            <script type="application/ld+json">
              {/* {JSON.stringify(breadcrumb)} */}
            </script>
          </Helmet>
        )
      }}
    />
  )
}

export default Metadata

Metadata.defaultProps = {
  title: null,
  desc: null,
  banner: null,
  pathname: null,
  meta: null,
}

const query = graphql`
  query SEO {
    site {
      buildTime(formatString: "YYYY-MM-DD")
      siteMetadata {
        siteUrl
        defaultTitle: title
        defaultDescription: description
        defaultBanner: banner
        headline
        siteLanguage
        ogLanguage
        author
      }
    }
  }
`
